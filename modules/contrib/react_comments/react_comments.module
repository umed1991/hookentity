<?php

use Drupal\node\NodeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\react_comments\Model\Comments as CommentsModel;
use Drupal\comment\Entity\Comment as CommentEntity;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;

define('RC_COMMENT_PUBLISHED', 1);
define('RC_COMMENT_UNPUBLISHED', 0);
define('RC_COMMENT_FLAGGED', 2);
define('RC_COMMENT_DELETED', 3);

function react_comments_preprocess_field(&$vars) {
  global $base_url;

  if ($vars['field_type'] === 'comment') {
    $vars['#attached']['library'][] = 'react_comments/react_comments';
    $vars['config'] = json_encode([
      'status' => isset($vars['element']['#object']->comment)
        ? $vars['element']['#object']->comment->status == CommentItemInterface::OPEN
        : FALSE,
      'entity_id' => $vars['element']['#object'] ? $vars['element']['#object']->id() : NULL,
      'origin' => $base_url,
      'full_delete' => \Drupal::config('react_comments.settings')->get('full_delete'),
      'notify' => _react_comments_comment_notify($vars)
    ]);

  }
}

/**
 * Implements hook_entity_delete().
 */
function react_comments_entity_delete(EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'comment') {
    // TODO: Drupal Rector Notice: Please delete the following comment after you've made any necessary changes.
    // You will need to use `\Drupal\core\Database\Database::getConnection()` if you do not yet have access to the container here.
    \Drupal::database()->delete('react_comments_status')
      ->condition('cid', $entity->id())
      ->execute();
  }
}

/**
 * Implements hook_node_links_alter().
 */
function react_comments_node_links_alter(array &$links, NodeInterface $entity, array &$context) {
  unset($links['comment__comment']);
}

/**
 * Implements hook_theme_registry_alter().
 */
function react_comments_theme_registry_alter(&$theme_registry) {
  $theme_registry['field__comment']['path'] = drupal_get_path('module', 'react_comments') . '/templates';
}

/**
 * Implements hook_theme().
 */
function react_comments_theme($existing, $type, $theme, $path) {
  return [
    'comments' => [
      'variables' => ['entity_id' => NULL, 'origin'    => NULL, 'is_hidden' => FALSE],
    ],
  ];
}

/**
 * Helper function to integrate with Comment Notify module.
 */
function _react_comments_comment_notify(&$vars) {
  if (!\Drupal::service('module_handler')->moduleExists('comment_notify')) {
    return FALSE;
  }

  $user = \Drupal::currentUser();
  if (!($user->hasPermission('subscribe to comments') || $user->hasPermission('administer comments'))) {
    return FALSE;
  }

  // Only add the checkbox if this is an enabled content type.
  $enabled_types = \Drupal::config('comment_notify.settings')->get('bundle_types');
  $type = implode('--', [
    $vars['element']['#entity_type'],
    $vars['element']['#bundle'],
    $vars['element']['#field_name'],
  ]);
  if (!in_array($type, $enabled_types)) {
    return FALSE;
  }

  // Add the checkbox for anonymous users.
  $entity = $vars['element']['#object'];
  $field = $entity->getFieldDefinition($vars['element']['#field_name']);
  $settings = $field->getSettings();
  if ($user->isAnonymous() && $settings['anonymous'] == COMMENT_ANONYMOUS_MAYNOT_CONTACT) {
    // If anonymous users can't enter their e-mail don't tempt them with the
    // checkbox.
    return FALSE;
  }

  /** @var \Drupal\comment_notify\UserNotificationSettings $user_settings */
  $user_settings = \Drupal::service('comment_notify.user_settings');
  $preference = intval($user_settings->getSetting($user->id(), 'comment_notify'));

  return [
    'default' => $preference != 0 ? $preference : -1,
    'types' => _comment_notify_options(),
  ];
}
