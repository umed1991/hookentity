<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/hookentity/templates/system/page--front.html.twig */
class __TwigTemplate_3698a9436fcdf4a69711c3b26e460aff96f6643459fe1792cadb836c57f6e1b0 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'services' => [$this, 'block_services'],
            'skills' => [$this, 'block_skills'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 54, "if" => 62, "block" => 72];
        $filters = ["escape" => 57];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 55
        echo "<!-- Navigation -->
<div class=\"menu-top\">
      ";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
  <a class=\"menu-toggle rounded\" href=\"#\">
    <i class=\"fa fa-bars\"></i>
  </a>
</div>
";
        // line 62
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 63
            echo "  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
";
        }
        // line 65
        echo "
";
        // line 67
        echo "<div class=\"container-fluid front-slider\">
  ";
        // line 68
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "front_slider", [])), "html", null, true);
        echo "
</div>

";
        // line 72
        $this->displayBlock('main', $context, $blocks);
        // line 131
        echo "
<div class=\"front-services ";
        // line 132
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" >
  <div class=\"row\">
    ";
        // line 135
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "services", [])) {
            // line 136
            echo "            ";
            $this->displayBlock('services', $context, $blocks);
            // line 141
            echo "          ";
        }
        // line 142
        echo "  </div>
</div>
<div class=\"front-skills ";
        // line 144
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" >
  <div class=\"row\">
    ";
        // line 147
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "skills", [])) {
            // line 148
            echo "            ";
            $this->displayBlock('skills', $context, $blocks);
            // line 153
            echo "          ";
        }
        // line 154
        echo "  </div>
</div>

";
        // line 157
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 158
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 72
    public function block_main($context, array $blocks = [])
    {
        // line 73
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 77
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 78
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 83
            echo "      ";
        }
        // line 84
        echo "
      ";
        // line 86
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 87
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 92
            echo "      ";
        }
        // line 93
        echo "
      ";
        // line 95
        echo "      ";
        // line 96
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 97
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 98
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 99
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 100
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 103
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 106
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 107
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 110
            echo "        ";
        }
        // line 111
        echo "
        ";
        // line 113
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 114
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 117
            echo "        ";
        }
        // line 118
        echo "      </section>

      ";
        // line 121
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 122
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 127
            echo "      ";
        }
        // line 128
        echo "    </div>
  </div>
";
    }

    // line 78
    public function block_header($context, array $blocks = [])
    {
        // line 79
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 87
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 88
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 89
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 107
    public function block_highlighted($context, array $blocks = [])
    {
        // line 108
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 114
    public function block_help($context, array $blocks = [])
    {
        // line 115
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 122
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 123
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 124
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 136
    public function block_services($context, array $blocks = [])
    {
        // line 137
        echo "              <div class=\"col-sm-12 homepage-services\">
                ";
        // line 138
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "services", [])), "html", null, true);
        echo "
              </div>
            ";
    }

    // line 148
    public function block_skills($context, array $blocks = [])
    {
        // line 149
        echo "              <div class=\"col-sm-12 our-skills\">
                ";
        // line 150
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "skills", [])), "html", null, true);
        echo "
              </div>
            ";
    }

    // line 158
    public function block_footer($context, array $blocks = [])
    {
        // line 159
        echo "    <footer class=\"footer ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
      ";
        // line 160
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/hookentity/templates/system/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 160,  324 => 159,  321 => 158,  314 => 150,  311 => 149,  308 => 148,  301 => 138,  298 => 137,  295 => 136,  288 => 124,  285 => 123,  282 => 122,  275 => 115,  272 => 114,  265 => 108,  262 => 107,  255 => 89,  252 => 88,  249 => 87,  242 => 80,  239 => 79,  236 => 78,  230 => 128,  227 => 127,  224 => 122,  221 => 121,  217 => 118,  214 => 117,  211 => 114,  208 => 113,  205 => 111,  202 => 110,  199 => 107,  196 => 106,  190 => 103,  188 => 100,  187 => 99,  186 => 98,  185 => 97,  184 => 96,  182 => 95,  179 => 93,  176 => 92,  173 => 87,  170 => 86,  167 => 84,  164 => 83,  161 => 78,  158 => 77,  151 => 73,  148 => 72,  142 => 158,  140 => 157,  135 => 154,  132 => 153,  129 => 148,  126 => 147,  121 => 144,  117 => 142,  114 => 141,  111 => 136,  108 => 135,  103 => 132,  100 => 131,  98 => 72,  92 => 68,  89 => 67,  86 => 65,  80 => 63,  78 => 62,  70 => 57,  66 => 55,  64 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/hookentity/templates/system/page--front.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/hookentity/themes/custom/hookentity/templates/system/page--front.html.twig");
    }
}
