<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/hookentity/templates/system/page.html.twig */
class __TwigTemplate_edeae5e5e1a6e1a5b552beab813c8626ca38aa37020c2e12a80814149c3f5fa6 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'header' => [$this, 'block_header'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 54, "if" => 66, "block" => 76];
        $filters = ["escape" => 59];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 54
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", []), "fluid_container", [])) ? ("container-fluid") : ("container"));
        // line 55
        echo "<!-- Navigation -->
<div class=\"menu-top\">
  <div class=\"logo-top row\">
    <div class=\"logo-wrapper col-sm-6 col-sm-offset-5\">
      ";
        // line 59
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
    </div>
  </div>
  <a class=\"menu-toggle rounded\" href=\"#\">
    <i class=\"fa fa-bars\"></i>
  </a>
</div>
";
        // line 66
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 67
            echo "  ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
";
        }
        // line 69
        echo "
";
        // line 71
        echo "<div class=\"container-fluid front-slider\">
  ";
        // line 72
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "front_slider", [])), "html", null, true);
        echo "
</div>

";
        // line 76
        $this->displayBlock('main', $context, $blocks);
        // line 141
        echo "
";
        // line 142
        if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
            // line 143
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 76
    public function block_main($context, array $blocks = [])
    {
        // line 77
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 81
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 82
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 87
            echo "      ";
        }
        // line 88
        echo "
      ";
        // line 90
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 91
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 96
            echo "      ";
        }
        // line 97
        echo "
      ";
        // line 99
        echo "      ";
        // line 100
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 101
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 102
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 103
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 104
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 107
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 110
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 111
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 114
            echo "        ";
        }
        // line 115
        echo "
        ";
        // line 117
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 118
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 121
            echo "        ";
        }
        // line 122
        echo "
        ";
        // line 124
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 128
        echo "      </section>

      ";
        // line 131
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 132
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 137
            echo "      ";
        }
        // line 138
        echo "    </div>
  </div>
";
    }

    // line 82
    public function block_header($context, array $blocks = [])
    {
        // line 83
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 84
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
          </div>
        ";
    }

    // line 91
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 92
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 93
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 111
    public function block_highlighted($context, array $blocks = [])
    {
        // line 112
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 118
    public function block_help($context, array $blocks = [])
    {
        // line 119
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 124
    public function block_content($context, array $blocks = [])
    {
        // line 125
        echo "          <a id=\"main-content\"></a>
          ";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 132
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 133
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 134
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 143
    public function block_footer($context, array $blocks = [])
    {
        // line 144
        echo "    <footer class=\"footer ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\" role=\"contentinfo\">
      ";
        // line 145
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
        echo "
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/hookentity/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  287 => 145,  282 => 144,  279 => 143,  272 => 134,  269 => 133,  266 => 132,  260 => 126,  257 => 125,  254 => 124,  247 => 119,  244 => 118,  237 => 112,  234 => 111,  227 => 93,  224 => 92,  221 => 91,  214 => 84,  211 => 83,  208 => 82,  202 => 138,  199 => 137,  196 => 132,  193 => 131,  189 => 128,  186 => 124,  183 => 122,  180 => 121,  177 => 118,  174 => 117,  171 => 115,  168 => 114,  165 => 111,  162 => 110,  156 => 107,  154 => 104,  153 => 103,  152 => 102,  151 => 101,  150 => 100,  148 => 99,  145 => 97,  142 => 96,  139 => 91,  136 => 90,  133 => 88,  130 => 87,  127 => 82,  124 => 81,  117 => 77,  114 => 76,  108 => 143,  106 => 142,  103 => 141,  101 => 76,  95 => 72,  92 => 71,  89 => 69,  83 => 67,  81 => 66,  71 => 59,  65 => 55,  63 => 54,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/hookentity/templates/system/page.html.twig", "/Applications/XAMPP/xamppfiles/htdocs/hookentity/themes/custom/hookentity/templates/system/page.html.twig");
    }
}
